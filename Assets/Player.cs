﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    private float speed = .1F;
    private GameObject playerObject;
    private Color playerColour = Color.white;
    public static List<Color> Colors;
    private int ColourChangeDelay = 300;
    private int ColourCounter = 0;
    private SpriteRenderer playerSpriteRenderer;
    private int Points = 0;

    // Use this for initialization
    void Start () {
        playerObject = GameObject.Find("Player"); //Find player object
        Colors = new List<Color>();
        Colors.Add(Color.blue);
		Colors.Add (Color.yellow);
        Colors.Add(Color.red);
        Colors.Add(Color.green);
        playerSpriteRenderer = playerObject.GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0))
        {
            Vector3 mousepos = Input.mousePosition; //Get actual mouse position
            mousepos.z = 0; //z has no meaning
            Vector3 mousepos_normalised = Camera.main.ScreenToWorldPoint(mousepos); //Transform to world coords
            playerObject.transform.Translate((mousepos_normalised.x - playerObject.transform.position.x) * speed, 0, 0); //Translate player to mouse with speed "speed"
        }
        if(ColourCounter == 0)
        {
            playerColour = Colors[Random.Range(0, Colors.Count)];
            playerSpriteRenderer.color = playerColour;
        }
        ColourCounter++;
        ColourCounter %= ColourChangeDelay;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        SpriteRenderer booperrend = coll.gameObject.GetComponent<SpriteRenderer>();
        if (booperrend.color == playerColour)
        { 
            playerObject.transform.localScale += new Vector3(0.7F, 0.7F);
            playerObject.transform.Translate(new Vector3(0, 0.07F));
            Points++;
        }
        else
        {
            playerObject.transform.localScale -= new Vector3(0.7F, 0.7F);
            playerObject.transform.Translate(new Vector3(0, -0.07F));
            Points--;
        }
        Debug.Log(Points);
        Destroy(coll.gameObject);
    }
}
