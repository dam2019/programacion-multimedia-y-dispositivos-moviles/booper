﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BooperCreator : MonoBehaviour {

    GameObject MotherBooper;//bola original
    List<GameObject> Boopers;//Lista de bolas que caen
    List<GameObject> DestroyList;//Lista de bolas para destruir
	int Counter = 0;//contador
    int Rate = 10; //Higher is SLOWER
    float speed = 0.1F;
    const float Top = 10F; //Top of the game - no idea what number this is so tweak
    const float Bottom = -1; //Bottom of game - again no idea so tweak
    const float xLimit = 10F; //Spawn limits horizontally;
    const int booperLimit = 10;

    // Use this for initialization
    void Start () {
        MotherBooper = GameObject.Find("booper");
        Boopers = new List<GameObject>();
        DestroyList = new List<GameObject>();

    }
	
	// Update is called once per frame
	void Update () {
        if(Counter == 0 && Boopers.Count <= booperLimit)
        {
            GameObject newBooper = (GameObject)Instantiate(MotherBooper, new Vector3(Random.Range(-xLimit, xLimit), Top), Quaternion.identity);
            SpriteRenderer booperrend = newBooper.GetComponent<SpriteRenderer>();
            booperrend.color = Player.Colors[Random.Range(0, Player.Colors.Count)];
            Boopers.Add(newBooper); //Add a new booper
            
        }

        foreach(GameObject booper in Boopers) //Check status of all boopers
        {
            if (booper != null) //May be null due to collison
            {
                booper.transform.Translate(new Vector3(0, -speed)); //Move the booper down
                if (booper.transform.position.y < Bottom) //If the booper is now off the screen, destroy it!
                {
                    DestroyList.Add(booper); //Add this booper to our destroy list
                }
            }
            else
            {
                DestroyList.Add(booper);
            }
        }

        foreach(GameObject delbooper in DestroyList) //Destroy all boopers on the list
        {
            Boopers.Remove(delbooper);
            if(delbooper != null)
            {
                Destroy(delbooper.gameObject);
            }
        }

        DestroyList.Clear(); //Remove all items from the delete list

        Counter++; //Increment counter
        Counter %= Rate; //When Counter == Rate, Counter = 0.
	}
}
